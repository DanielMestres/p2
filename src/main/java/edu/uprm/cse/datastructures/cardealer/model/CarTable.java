package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.Map;

public class CarTable {
	private static Map<Long,Car> table = new HashTableOA<Long,Car>(new LongComparator(), new CarComparator());

	/*
	 * Returns the table
	 */
	public static Map<Long,Car> getInstance() {
		return table;
	}

	/*
	 * Instances a new table
	 */
	public static void resetCars() { 
		table =  new HashTableOA<Long,Car>(new LongComparator(), new CarComparator());
	}
}