package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

public class HashTableOA<K,V> implements Map<K,V> {
	/*
	 * MapEntry Class
	 */
	private static class MapEntry<K,V> {
		private K key;
		private V value;
		private boolean active;
		
		public MapEntry(K key, V value, boolean active) {
			this.key = key;
			this.value = value;
			this.active = active;
		}

		public K getKey() {
			return key; 
		}
		
		public V getValue() {
			return value; 
		}
		
		public boolean isActive() {
			return active;
		}
		
		public void setMapEntry(K key, V value, boolean active) {
			this.key = key;
			this.value = value; 
			this.active = active;
		}
	}

	/*
     * Class Variables
     */
	private static final int DEFAULT_CAPACITY = 5;
	
	private int currentSize;
	private Object[] entries;
	private Comparator<K> kComp;
	private Comparator<V> vComp;

	/*
     * Constructors
     */
	public HashTableOA(int initialCapacity, Comparator<K> kComp, Comparator<V> vComp) {
		if(initialCapacity <= 0)
			initialCapacity = DEFAULT_CAPACITY;
			
		this.currentSize = 0;
		this.entries = new Object[initialCapacity];
		this.kComp = kComp;
		this.vComp = vComp;
		
		int i = 0;
		while(i < this.entries.length) {
			this.entries[i++] = new MapEntry<K,V>(null, null, false);
		}
	}
	
	public HashTableOA(Comparator<K> kComp, Comparator<V> vComp) {
		this(DEFAULT_CAPACITY, kComp, vComp);
	}
	
	/*
     * Utility Methods
     */
	private int firstHash(K key) {
		return (key.hashCode() * 8) % this.entries.length;
	}
	
	private int secondHash(K key) {
		return 8 - (firstHash(key) % 8);
	}
	
	@SuppressWarnings("unchecked")
	private void reSize() {
		Object[] oldTable = this.entries;
		this.entries = new Object[this.size() * 2];
		this.currentSize = 0;
		int count = 0;
		
		while(count < this.entries.length) {
			this.entries[count++] = new MapEntry<K,V>(null,null,false);
		}
		
		count = 0;
		
		while(count < oldTable.length) {
			this.put(((MapEntry<K, V>) oldTable[count++]).getKey(), ((MapEntry<K, V>) oldTable[count++]).getValue());
		}
	}
	
	/*
     * Class Methods
     */
	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public boolean contains(K key) {
		return this.get(key) != null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(K key) {
		if(!(key == null)) {
			int target = firstHash(key);
			
			if(((MapEntry<K, V>) entries[target]).isActive() && ((MapEntry<K, V>) entries[target]).getKey().equals(key))
				return ((MapEntry<K, V>) entries[target]).getValue();
			else {
				// Rehashes if target is not found
				target = secondHash(key) % entries.length;
				
				if(((MapEntry<K, V>) entries[target]).isActive() && ((MapEntry<K, V>) entries[target]).getKey().equals(key))
					return ((MapEntry<K, V>) entries[target]).getValue();
				else {
					// Uses linear probing if target is not found after the second hash
					int temp = (target + 1) % this.entries.length;
					
					while(temp != target) {
						if(((MapEntry<K, V>) this.entries[temp]).isActive() && ((MapEntry<K, V>) this.entries[temp]).getKey().equals(key))
							return ((MapEntry<K, V>) this.entries[temp]).getValue();
						temp = (temp + 1) % this.entries.length;
					}
					// Returns null if target is not found after linear probing
					return null;
				}
			}
		}
		else
			throw new IllegalArgumentException("Argument cannot be null");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public V put(K key, V value) {
		if(key == null || value == null)
			throw new IllegalArgumentException("Argument cannot be nulll");
		
		// Store original value to return it
		V oldValue = this.remove(key);
		
		if(this.size() == this.entries.length)
			this.reSize();
		
		int target = firstHash(key);
	
		if(!((MapEntry<K,V>) this.entries[target]).isActive()) {
			((MapEntry<K,V>) this.entries[target]).setMapEntry(key, value, true);
			this.currentSize++;
		} else {
			// Rehashes if target is active
			target = (target + secondHash(key)) % entries.length;
			
			if(!((MapEntry<K,V>) this.entries[target]).isActive()) {
				((MapEntry<K,V>) this.entries[target]).setMapEntry(key, value, true);
				this.currentSize++;
			} else {
				// Uses linear probing if target is active after the second hash
				int temp = (target + 1) % this.entries.length;
					
				while(temp != target) {
					if(!((MapEntry<K, V>)this.entries[temp]).isActive()) {
						((MapEntry<K, V>)this.entries[temp]).setMapEntry(key, value, true);
						this.currentSize++;
					}
					temp = (temp + 1) % this.entries.length;
				}
			}
		}
		return oldValue;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public V remove(K key) {
		if(key == null)
			throw new IllegalArgumentException("Key cannot be null.");
		
		int target = firstHash(key);
		
		if(((MapEntry<K, V>) entries[target]).isActive() && ((MapEntry<K, V>) entries[target]).getKey().equals(key)) {
			V oldValue = ((MapEntry<K, V>) entries[target]).getValue();
			((MapEntry<K, V>) entries[target]).setMapEntry(null, null, false);
			currentSize--;
			return oldValue;
		}
		else {
			// Rehashes if target is not found
			target = (target + secondHash(key)) % entries.length;
			
			if(((MapEntry<K, V>) entries[target]).isActive() && ((MapEntry<K, V>) entries[target]).getKey().equals(key)) {
				V oldValue = ((MapEntry<K, V>) entries[target]).getValue();
				((MapEntry<K, V>) entries[target]).setMapEntry(null, null, false);
				currentSize--;
				return oldValue;
			}
			else {
				// Uses linear probing if target is not found after the second hash
				int temp = (target + 1) % this.entries.length;
				
				while(temp != target) {
					if(((MapEntry<K, V>)this.entries[temp]).isActive() && ((MapEntry<K, V>)this.entries[temp]).getKey().equals(key)) {
						V oldValue = ((MapEntry<K, V>)this.entries[temp]).getValue();
						((MapEntry<K, V>)this.entries[temp]).setMapEntry(null, null, false);
						this.currentSize--;
						return oldValue;
					}
					
					temp = (temp + 1) % this.entries.length;
				}
				// Returns null if target is not found after linear probing
				return null;
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public SortedList<K> getKeys() {
		SortedList<K> result = new CircularSortedDoublyLinkedList<K>(kComp);
		int count = 0;
		
		while(count < entries.length) {
			if(((MapEntry<K,V>) entries[count]).isActive()) {
				result.add(((MapEntry<K,V>) entries[count]).getKey());
				count++;
			} else
				count++;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SortedList<V> getValues() {
		SortedList<V> result = new CircularSortedDoublyLinkedList<V>(vComp);	
		int count = 0;
		
		while(count < entries.length) {
			if(((MapEntry<K,V>) entries[count]).isActive()) {
				result.add(((MapEntry<K,V>) entries[count]).getValue());
				count++;
			} else
				count++;
		}
		return result;
	}
}