package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	/*
	 * Node Class
	 */
	private static class Node<E> {
        private E element;
        private Node<E> prev;
        private Node<E> next;
        
        Node(E element, Node<E> next) {
            this.element = element;
            this.setNext(next);
        }
        
        Node() {
            this.element = null;
            this.prev = null;
            this.next = null;
        }
        
        E getElement() {
            return this.element;
        }
        
        Node<E> getPrev() {
            return this.prev;
        }
        
        Node<E> getNext() {
            return this.next;
        }

        @SuppressWarnings("unused")
		void setElement(E element) {
            this.element = element;
        }

        void setPrev(Node<E> previous) {
            this.prev = previous;
        }

        void setNext(Node<E> next) {
            this.next = next;
            next.setPrev(this);
        }
        
        void clearElement() {
        	this.element = null;
        }
        
        void clearNext() {
        	this.next = null;
        }
        
        void clearPrev() {
        	this.prev = null;
        }
    }

    /*
     * Iterator Class
     */
    @SuppressWarnings("hiding")
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E> {
        private Node<E> next;

        @SuppressWarnings("unchecked")
		CircularSortedDoublyLinkedListIterator() {
            this.next = (Node<E>) header.getNext();
        }
        
        @Override
        public boolean hasNext() {
            return this.next != header;
        }

        @Override
        public E next() {
            if(this.hasNext()) {
                E result = this.next.getElement();
                this.next = this.next.getNext();
                return result;
            } else {
                throw new NoSuchElementException();
            }
        }
    }
    
    /*
     * Iterator method
     */
    @Override
    public Iterator<E> iterator() {
        return new CircularSortedDoublyLinkedListIterator<E>();
    }
    
    /*
     * Class Variables
     */
    private Node<E> header;
    private int currentSize = 0;
    private Comparator<E> comparator;

    /*
     * Constructors
     */
    public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
        this.comparator = comparator;
        this.header = new Node<E>();
        this.header.setNext(header);
    }

    /*
     * Class Methods
     */
    @Override
    public boolean add(E obj) {
        Node<E> temp = this.header;
        while(temp.getNext() != this.header && this.comparator.compare(temp.getNext().getElement(), obj) < 0) {
            temp = temp.getNext();
        }
        
        temp.setNext(new Node<E>(obj, temp.getNext()));
        this.currentSize++;
        return true;
    }

    @Override
    public int size() {
        return this.currentSize;
    }

    @Override
    public boolean remove(E obj) {
        int index = this.firstIndex(obj);
        if (index >= 0) {
            return this.remove(index);
        } else
        	return false;
    }

    @Override
    public boolean remove(int index) {
        if ((index < 0) || (index >= this.currentSize)) {
            throw new IndexOutOfBoundsException();
        } else {
	        Node<E> temp = getPosition(index);
	        temp.getPrev().setNext(temp.getNext());
	        temp.clearElement();
	        temp.clearNext();
	        temp.clearPrev();
	        this.currentSize--;
	        return true;
        }
    }
    
    /*
     * Helper Method
     */
    public Node<E> getPosition(int index) {
        int currentPos = 0;
        Node<E> temp = this.header.getNext();
        while(currentPos != index) {
            temp = temp.getNext();
            currentPos++;
        }
        return temp;
    }

    @Override
    public int removeAll(E obj) {
        int count = 0;
        while(this.remove(obj) != false) {
            count++;
        }
        return count;
    }

    @Override
    public E first() {
        return this.header.getNext().getElement();
    }

    @Override
    public E last() {
        return this.header.getPrev().getElement();
    }

    @Override
    public E get(int index) {
        if ((index < 0) || index >= this.currentSize) {
            throw new IndexOutOfBoundsException();
        } else {
            return this.getPosition(index).getElement();
        }
    }
    
    @Override
    public void clear() {
        while(this.header.getNext() != this.header) {
            this.remove(0);
        }
    }

    @Override
    public boolean contains(E e) {
        return this.firstIndex(e) > 0;
    }

    @Override
    public boolean isEmpty() {
        return this.currentSize == 0;
    }

    @Override
    public int firstIndex(E e) {
        int index = 0;
        Node<E> temp = this.header.getNext();
        while(temp != this.header) {
        	if(temp.getElement().equals(e)) {
                return index;
            }
        	temp = temp.getNext();
        	index++;
        }
        
        // Returns -1 if not found
        return -1;
    }

    @Override
    public int lastIndex(E e) {
        int index = this.currentSize -1;
        Node<E> temp = this.header.getPrev();
        while(temp != this.header) {
        	if(temp.getElement().equals(e)) {
                return index;
            }
        	temp = temp.getPrev();
        	index--;
        }
        
        // returns -1 if not found
        return -1;
    }
}