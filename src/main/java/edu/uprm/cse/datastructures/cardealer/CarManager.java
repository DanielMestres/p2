package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.Map;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	private final Map<Long,Car> table = CarTable.getInstance();

	/*
	 * Returns an array containing the cars in the table
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] result = new Car[table.size()];
		SortedList<Car> list = table.getValues();
		int count = 0;
		
		for(Car e : list) {
			result[count++] = e;
		}
		return result;
	}
	
	/*
	 * Returns car with given id, returns an error if car entry is null
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		if (!(table.get(id) == null))
			return table.get(id);
		else
			throw new WebApplicationException(Response.Status.NOT_FOUND);
	}

	/*
	 * Adds car to table, returns an error if car is null
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		if (!car.equals(null)) {
			table.put(car.getCarId(), car);
			return Response.status(Response.Status.CREATED).build();
		} else
			return Response.status(Response.Status.BAD_REQUEST).build();
	}
	
	/*
	 * Deletes car with given id if its present, returns an error if car entry is null
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		if (!(table.get(id) == null)) {
			table.remove(table.get(id).getCarId());
			return Response.status(Response.Status.OK).build();
		} else
			return Response.status(Response.Status.NOT_FOUND).build();
	}

	/*
	 * Replaces car that matches given id with the given car, returns an error if car is null
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(@PathParam("id") long id, Car car) {
		if (!car.equals(null)) {
			table.put(car.getCarId(), car);
			return Response.status(Response.Status.OK).build();
		} else
			return Response.status(Response.Status.NOT_FOUND).build();
	}
}